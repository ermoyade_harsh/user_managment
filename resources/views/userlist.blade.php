@extends('common.layout')

@section('title')
    {{$page_title}}
@endsection

@section('content')
    <div class="row">

        <div class="col-md-12 col-page-title">

            <div class="row">
                <div class="col-xs-6">
                    <h1 class="dashboard-page-title">User</h1>
                </div>
                <div class="col-xs-6">
                    <a href="{{url('/add-user')}}" class="btn bg-primary float-right">Add User</a>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            @if(Session::has('success'))
                <div class="alert alert-success" id="display_msg" style="display: none;">
                    <strong>{{ Session::get('success') }}</strong>
                </div>
            @endif

            <table class="table table-bordered" id="users-table">
                <thead>
                    <tr>
                        <th style="width: 60px;">Id</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Email</th>
                        <th>Mobile</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <thead >
                    <tr class="table-search">
                        <th style="width: 60px;">Id</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Email</th>
                        <th>Mobile</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>


    @push('scripts')
        <script>
            var ajaxRequestUsers = '<?php echo $ajaxRequestUsers;?>';
        </script>
        <script src="{{url('/js/common.js')}}"></script>
        <script src="{{url('/js/usersList.js')}}"></script>
        @if(Session::has('error') || Session::has('success') || count($errors) > 0)
            <script>
                setTimeout(function () {
                    $("#display_msg").fadeOut();
                }, 20000);
                setTimeout(function () {
                    $("#display_msg").fadeIn();
                }, 200);

            </script>
        @endif
    @endpush

@endsection

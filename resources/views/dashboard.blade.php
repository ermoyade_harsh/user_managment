@extends('common.layout')

@section('title')
    {{$page_title}}
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-page-title">
            <div class="row">
                <div class="col-xs-6">
                    <h1 class="dashboard-page-title">Dashboard</h1>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            @if(Session::has('success'))
                <div class="alert alert-success" id="display_msg" style="display: none;">
                    <strong>{{ Session::get('success') }}</strong>
                </div>
            @endif
            <div class="row">
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header">Users</div>
                        <div class="card-body">{{$users}}</div>
                        <div class="card-footer"><a href="{{url('/users')}}">View Users</a></div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header">Roles</div>
                        <div class="card-body">{{$roles}}</div>
                        <div class="card-footer"><a href="{{url('/roles')}}">View Roles</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @push('scripts')
        @if(Session::has('error') || Session::has('success') || count($errors) > 0)
            <script>
                setTimeout(function () {
                    $("#display_msg").fadeOut();
                }, 20000);
                setTimeout(function () {
                    $("#display_msg").fadeIn();
                }, 200);
            </script>
        @endif
    @endpush
@endsection

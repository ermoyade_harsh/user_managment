@extends('common.layout')

@section('title')
    {{$page_title}}
@endsection
@push('style')
    <link rel="stylesheet" href="{{url('/css/jquery-ui.css')}}">
    <link rel="stylesheet" href="{{url('/css/jquery.datetimepicker.css')}}">
@endpush
@section('content')
    <div class="row">

        <div class="col-md-12 col-page-title">

            <div class="row">
                <div class="col-xs-12">
                    <h1 class="dashboard-page-title">Edit User</h1>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            @if (count($errors) > 0)
                <div class="alert alert-danger" id="display_msg">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @if(Session::has('error'))
                <div class="alert alert-dismissable alert-danger" id="display_msg" style="display: none;">
                    {!! Session::get('error') !!}
                </div>
            @endif


            <form action="{{url('/edit-user')}}" method="post" enctype="multipart/form-data" id="validation-form-demo" name="validation-form-demo" >
                <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                <input name="userid" type="hidden" value="{{ $user->id }}"/>
                <input name="oldemail" type="hidden" value="{{ $user->email }}"/>
                <input name="oldphone" type="hidden" value="{{ $user->phone }}"/>
                <div class="form-group col-sm-6">
                    <label>Name</label>
                    <input type="text" class="form-control" id="name" required name="name" placeholder="Name" value="{{ $user->name }}" >
                </div>
                <div class="form-group col-sm-6">
                    <label>Email</label>
                    <input type="email" class="form-control" id="email" required name="email" placeholder="Email"  value="{{ $user->email }}">
                </div>
                <div class="form-group col-sm-6">
                    <label for="phone">Mobile No.</label>
                    <input type="text" class="form-control" id="phone" value="{{ $user->phone }}" required name="phone" placeholder="Mobile No"  data-parsley-minlength="10" data-parsley-maxlength="10" data-parsley-maxlength-message="Max Length 10 number" data-parsley-minlength-message="Min Length 10 number" data-parsley-type="digits" data-parsley-type-message="only numbers">
                </div>
                <div class="row card-body-space">
                    <div class="col-sm-12">
                        <input type="submit" class="btn--small btn-primary pull-right" id="save"  value="Edit User">
                    </div>
                </div>
            </form>
        </div>
    </div>


    @push('scripts')
        <script src="{{url('/js/jquery-ui.js')}}"></script>
        <script src="{{url('/js/jquery.datetimepicker.full.js')}}"></script>
        <script src="{{url('/js/parsley.min.js')}}"></script>
        <script src="{{url('/js/validation.js')}}"></script>
        @if(Session::has('error') || count($errors) > 0)
            <script>
                setTimeout(function () {
                    $("#display_msg").fadeOut();
                }, 20000);
                setTimeout(function () {
                    $("#display_msg").fadeIn();
                }, 200);

            </script>
        @endif
    @endpush

@endsection

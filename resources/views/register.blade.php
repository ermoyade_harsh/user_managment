<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{url('/css/style.css')}}">
    <link rel="stylesheet" href="{{url('/css/parsley.css')}}">
    <link rel="stylesheet" href="{{url('/css/jquery-ui.css')}}">
    <link rel="stylesheet" href="{{url('/css/jquery.datetimepicker.css')}}">
    <style type="text/css">
        .row{
            width: 100%;
        }
        .parsley-required{
            position: relative;
            top: -13px;
        }
    </style>
</head>
<body>
    {{ csrf_field() }}
    <nav class="navbar navbar-expand-sm bg-light">
        <div class="container-fluid">
            <div class="row">
                <div class="col-6">
                    <a class="nav-link" href="{{url('/')}}">
                        <span style="font-weight: 100;">User Managment</span>
                    </a>
                </div>
                <div class="col-6">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link" href="{{url('/')}}">Login</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <div class="container-fluid container-fluid--content">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12 col-page-title">
                        <div class="row">
                            <div class="col-xs-12 offset-6">
                                <h1 class="dashboard-page-title">Register</h1>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger" id="display_msg">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{!! $error !!}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if(Session::has('error'))
                            <div class="alert alert-dismissable alert-danger" id="display_msg" style="display: none;">
                                {!! Session::get('error') !!}
                            </div>
                        @endif
                        <form action="{{url('/register')}}" method="post" class="offset-5" id="validation-form-demo" name="validation-form-demo" enctype="multipart/form-data">
                            <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                            <div class="form-group col-sm-6">
                                <label>First Name</label>
                                <input type="text" class="form-control" id="first_name" required name="first_name" placeholder="First Name" value="{{ old('first_name') }}" >
                            </div>
                            <div class="form-group col-sm-6">
                                <label>Last Name</label>
                                <input type="text" class="form-control" id="last_name" required name="last_name" placeholder="Last Name" value="{{ old('last_name') }}" >
                            </div>
                            <div class="form-group col-sm-6">
                                <label>Email</label>
                                <input type="email" class="form-control" id="email" required name="email" placeholder="Email" value="{{ old('email') }}" >
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="phone">Mobile No.</label>
                                <input type="text" class="form-control" id="phone" value="{{ old('phone') }}" required name="phone" placeholder="Mobile No"  data-parsley-minlength="10" data-parsley-maxlength="10" data-parsley-maxlength-message="Max Length 10 number" data-parsley-minlength-message="Min Length 10 number" data-parsley-type="digits" data-parsley-type-message="only numbers">
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="password">Password</label>
                                <input type="password" class="form-control" id="password" required name="password" placeholder="Password" value="{{ old('password') }}">
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="cpassword">Confirm Password</label>
                                <input type="password" class="form-control" id="cpassword" required name="cpassword" placeholder="Confirm Password" value="{{ old('cpassword') }}" data-parsley-equalto="#password">
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="department_id">Roles</label>
                                <select name="roles_id" id="roles_id" class="form-control" style="padding: 3px; height: 34px;" required>
                                    <option value="">--Select Roles--</option>
                                    @foreach($roles as $role)
                                        <option value="{{$role->id}}">{{$role->role_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="photo">Profile</label>
                                <input type="file" class="form-control" id="photo" required name="photo" data-parsley-max-File-Size1="" data-parsley-max-file-size="2048" >
                                <div class="previewImageWrapper">

                                </div>
                            </div>
                            <div class="row card-body-space">
                                <div class="col-sm-12">
                                    <button type="submit" class="btn--small btn-primary pull-right" id="save"  value="Add User">Add User</button>
                                </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="container-fluid container-fluid--footer">
        <div class="row">
            <div class="col-md-12">

            </div>
        </div>
    </div>
    <input type="hidden" name="url_base" id="url_base" value="{{url('/')}}">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="{{url('/js/config.js')}}"></script>
    <script src="{{url('/js/jquery-ui.js')}}"></script>
    <script src="{{url('/js/jquery.datetimepicker.full.js')}}"></script>
    <script src="{{url('/js/parsley.min.js')}}"></script>
    <script src="{{url('/js/register_validation.js')}}"></script>
    @if(Session::has('error') || count($errors) > 0)
        <script>
            setTimeout(function () {
                $("#display_msg").fadeOut();
            }, 20000);
            setTimeout(function () {
                $("#display_msg").fadeIn();
            }, 200);

        </script>
    @endif
</body>
</html>

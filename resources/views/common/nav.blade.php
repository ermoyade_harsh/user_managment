<nav class="navbar navbar-expand-sm bg-light">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-6">
                <a class="nav-link" href="{{url('/')}}">
                    <span style="font-weight: 100;">User Managment</span>
                </a>
            </div>
            <div class="col-xs-6">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/dashboard')}}">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/users')}}">Users</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/roles')}}">Roles</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/logout')}}">Logout</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>

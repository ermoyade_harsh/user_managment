<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{url('/css/style.css')}}">
    <link rel="stylesheet" href="{{url('/js/DataTables/datatables.min.css')}}">
    <link rel="stylesheet" href="{{url('/css/toastr.min.css')}}">
    <link rel="stylesheet" href="{{url('/css/sweetalert.min.css')}}">
    <link rel="stylesheet" href="{{url('/css/parsley.css')}}">
    @stack('style')
    <style type="text/css">
        .row{
            width: 100%;
        }
        .serchSelect{
            width: 100%;
            padding: 3px;
        }
    </style>
</head>
<body>
{{ csrf_field() }}
@include('common.nav')

<div class="container-fluid container-fluid--content">
    <div class="row">
        <div class="col-md-12">

            @yield('content')
        </div>
    </div>
</div>


<div class="container-fluid container-fluid--footer">
    <div class="row">
        <div class="col-md-12">

        </div>
    </div>
</div>
<input type="hidden" name="url_base" id="url_base" value="{{url('/')}}">

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="{{url('/js/config.js')}}"></script>
<script src="{{url('/js/DataTables/datatables.min.js')}}"></script>
<script src="{{url('/js/sweetalert.min.js')}}"></script>
<script src="{{url('/js/toastr.min.js')}}"></script>

@stack('scripts')
</body>
</html>
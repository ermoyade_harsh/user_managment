<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    protected $table = 'users';
    protected $primaryKey = 'id';

    public function getTotalUser(){
        $result = self::count();
        return $result;
    }

    public function getUserDetails($id){
        $result = self::where('id', '=', $id)->first();
        return $result;
    }

    public function getAllUser(){
        $result = self::get();
        return $result;
    }

    public function deleteUser($id){
        $result = self::where('id', '=', $id)->delete();
        return $result;
    }

    public function checkMail($email){
        $found = self::select('id')
            ->where('email', '=', $email)
            ->first();
        return $found;
    }

    public function checkPhone($phone){
        $found = self::select('id')
            ->where('phone', '=', $phone)
            ->first();
        return $found;
    }

    public function addUser($data){
        $userData = new Users;
        $userData->first_name = $data['first_name'];
        $userData->last_name = $data['last_name'];
        $userData->email = $data['email'];
        $userData->password = md5($data['password']);
        $userData->phone = $data['phone'];
        $userData->role_id = $data['roles_id'];
        $userData->image = $data['image'];
        $insertData = $userData->save();
        if($insertData){
            $response['status'] = true;
            $response['id']= $userData['id'];
            $response['userData']= $userData;
        }else{
            $response['status'] = false;
        }
        return $response;
    }

    public function updateUserStatus($id){
        $userData               = Users::where('id', $id)->first();
        $userData->status    = 1;
        $updated                = $userData->save();
        if( $updated ) return true;
        return false;
    }
}

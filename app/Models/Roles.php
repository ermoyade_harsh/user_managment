<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Self_;

class Roles extends Model
{
    protected $table = 'roles';
    protected $primaryKey = 'id';

    public function getAllRoles(){
        $found = Self::get();
        return $found;
    }

    public function getTotalRoles(){
        $found = Self::count();
        return $found;
    }
}

<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class BaseModel {

    protected $connection = null;

    public function __construct(){

        $this->connection = DB::connection()->getPdo();
    }

}
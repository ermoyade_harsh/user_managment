<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Users;
use App\Models\Roles;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use File;
use Session;
use Carbon\Carbon;
use DB;
use DirectoryIterator;

class LoginController extends Controller
{

    private $users;
    private $roles;

    public function __construct(Users $users, Roles $roles){
        $this->users  = $users;
        $this->roles  = $roles;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $data['page_title'] = 'Welcome!';
        return view('login')->with($data);
    }

    public function getLogin(Request $request){
        $data = Input::all();
        $messages = array(
            'email.required' => 'User Name is Required Field.',
            'password.required' => 'Password is Required Field',
        );
        $validationRules=[
            'email' => 'required',
            'password' => 'required',
        ];

        $validator = \Validator::make($data, $validationRules,$messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput(Input::all());
        }

        $query = DB::table('users as u')
            ->leftjoin('roles as r', 'r.id', '=', 'u.role_id')
            ->select(['u.*', 'r.role_type', 'r.role_name'])
            ->where('u.status', '=', '1')
            ->where('u.email', '=', $request['email'])
            ->where('u.password', '=', md5($request['password']))->first();
        if($query){
            session([
                'is_logged_in' => TRUE,
                'user_id' => $query->id,
                'email' => $query->email,
                'role_type' => $query->role_type,
            ]);
            return redirect('/dashboard');
        }else{
            Session::flash('error','please enter valid email and password');
            return redirect()->back()->withInput(Input::all());
        }
    }

    public function logout(Request $request){
        $request->session()->flush();
        $response = redirect("/");
        Session::flash('success','Logged out successfully.');
        return $response->header('Cache-Control', 'nocache, no-store, max-age=0, must-revalidate')
            ->header('Pragma', 'no-cache')
            ->header('Expires', 'Fri, 01 Jan 1990 00:00:00 GMT');
    }

    public function addUserForm(){
        $data['page_title'] = 'Welcome!';
        $data['roles'] = $this->roles->getAllRoles();
        return view('register')->with($data);
    }

    public function addUser(Request $request){
        $data = Input::all();        
        $messages = array(
            'first_name.required' => 'First Name is Required Field.',
            'last_name.required' => 'Last Name is Required Field.',
            'email.required' => 'Email is Required Field',
            'phone.required' => 'Mobile No. is Required Field',
            'phone.numeric' => 'Mobile No. must be Numeric.',
            'password.required' => 'Password is Required Field.',
            'cpassword.required' => 'confirm Password is Required Field.',
            'roles_id.required' => 'Role Id is Required Field.',
        );
        $validationRules=[
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'phone' => 'required|numeric',
            'password' => 'required',
            'cpassword' => 'required',
            'roles_id' => 'required'
        ];

        $validator = \Validator::make($data, $validationRules,$messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput(Input::all());
        }

        if($data['email'] != ""){
            $checkEmail = $this->users->checkMail($data['email']);
            if(($checkEmail != null) || ($checkEmail != "" )){
                Session::flash('error','User Email already exist');
                return redirect()->back()->withInput(Input::all());
            }
        }

        if($data['phone'] != ""){
            $checkMobile = $this->users->checkPhone($data['phone']);
            if(($checkMobile != null) || ($checkMobile != "" )){
                Session::flash('error','User Mobile No already exist');
                return redirect()->back()->withInput(Input::all());
            }
        }

        $imageUrl = $request->file('photo');

        if(!empty($imageUrl)){
            if(File::size($imageUrl) > 2097152){
                Session::flash('error','User Profile upto 2 Mb max.');
                return redirect()->back()->withInput(Input::all());
            }
            $ext=$imageUrl->getClientOriginalExtension();
            if(!in_array(strtolower($ext),array("png","jpeg","jpg","gif"))){
                Session::flash('error','User Profile must be a PNG, JPEG, GIF file.');
                return redirect()->back()->withInput(Input::all());
            }
        }

        if($request->file('photo')!=""){
            $path = public_path() . "/upload";
            File::makeDirectory($path, $mode = 0777, true, true);
            $images = $request->file('photo');
            $imageName = time().'.'.$images->getClientOriginalExtension();
            $images->move(public_path('upload'), $imageName);
            $data['image'] = $imageName;
        }

        $response = $this->users->addUser($data);
        if($response['status'] === true){
            $email = $data['email'];
            /*\Mail::send('email_temp.email', ['data' =>$response], function ($m) use ($email,$data) {
                $m->from('no-reply@gmail.com', 'Harsh Moyade');
                $m->to($email)->subject('Active your Account');
            });*/
            Session::flash('success','User Added Successfull');
            return redirect('/');
        }else{
            Session::flash('error','Something went wrong');
            return redirect()->back()->withInput(Input::all());
        }
    }

    public function getActiveUser($id){
        $checkUser = $this->users->getUserDetails($id);

        if(($checkUser == null) || ($checkUser == "" )){
            Session::flash('error','User Not Fount');
            return redirect('/');
        }
        $updateStatus = $this->users->updateUserStatus($id);
        if($updateStatus === true){
            Session::flash('success','User Status Updated Successfull');
            return redirect('/');
        }else{
            Session::flash('error','Something went wrong');
            return redirect()->back()->withInput(Input::all());
        }
    }
}

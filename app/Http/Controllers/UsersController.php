<?php

namespace App\Http\Controllers;

use App\Models\Roles;
use App\Models\Users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use File;
use Session;
use Carbon\Carbon;
use DB;

class UsersController extends Controller
{

    private $users;
    private $roles;

    public function __construct(Users $users, Roles $roles){
        $this->users  = $users;
        $this->roles  = $roles;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $data['page_title'] = 'Users List';
        $data['ajaxRequestUsers'] = route('ajaxRequestUsers');
        return view('userlist')->with($data);
    }

    public function getAjaxUsersList(Request $request){
        $query = DB::table('users')->select(['id', 'first_name','last_name', 'email', 'phone']);
        $dt = datatables()::of($query);
        return $dt->make(false);
    }

    public function edit($id){
        $data['page_title'] = 'Edit User';
        $data['user'] = $this->users->getUserDetails($id);
        return view('edit_user')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request){
        $data = Input::all();
        $messages = array(
            'name.required' => 'Name is Required Field.',
            'email.required' => 'Email is Required Field',
            'phone.required' => 'Mobile No. is Required Field',
            'phone.numeric' => 'Mobile No. must be Numeric.',
        );
        $validationRules=[
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required|numeric',
        ];


        $validator = \Validator::make($data, $validationRules,$messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput(Input::all());
        }
        if($data['oldemail'] != $data['email']){
            if($data['email'] != ""){
                $checkEmail = DB::table('employee')
                    ->select('id')
                    ->where('email', '=', $data['email'])
                    ->first();
                if(($checkEmail != null) || ($checkEmail != "" )){
                    Session::flash('error','Employee Email already exist');
                    return redirect()->back()->withInput(Input::all());
                }

            }
        }

        if($data['oldphone'] != $data['phone']) {
            if ($data['phone'] != "") {
                $checkMobile = DB::table('employee')
                    ->select('id')
                    ->where('phone', '=', $data['phone'])
                    ->first();
                if (($checkMobile != null) || ($checkMobile != "")) {
                    Session::flash('error', 'Employee Mobile No already exist');
                    return redirect()->back()->withInput(Input::all());
                }

            }
        }

        $query = DB::table('user')
            ->where('id', '=' ,$data['userid'])
            ->update([
                'name' => $data['name'],
                'email' => $data['email'],
                'phone' => $data['phone'],
                'updated_at'=> Carbon::now()
            ]);

        if($query){
            Session::flash('success','User updated Successfull');
            return redirect('/users');
        }else{
            Session::flash('error','Something went wrong');
            return redirect()->back()->withInput(Input::all());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $compDetails = $this->company->getUserAllCompany($id);
        foreach ($compDetails as $_compDetails){
            $logo = $_compDetails->logo;
            File::deleteDirectory(public_path('upload/' . $logo));
        }
        $result = $this->users->deleteUser($id);
        $result = $this->company->deleteUsersAllCompany($id);
        return $result;
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Users;
use App\Models\Roles;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use File;
use Session;
use Carbon\Carbon;
use DB;
use DirectoryIterator;

class DashboardController extends Controller
{

    private $users;
    private $roles;

    public function __construct(Users $users, Roles $roles){
        $this->users  = $users;
        $this->roles  = $roles;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $data['page_title'] = 'Welcome!';
        $data['users'] = $this->users->getTotalUser();
        $data['roles'] = $this->roles->getTotalRoles();
        return view('dashboard')->with($data);
    }

}

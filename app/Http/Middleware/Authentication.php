<?php

namespace App\Http\Middleware;

use Closure;

class Authentication
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $fullUrl = $request->fullUrl();
        $sessionKeyExists = $request->session()->exists('is_logged_in');
        $sessionKey = $request->session()->get('is_logged_in');

        if(!$sessionKeyExists & (TRUE != $sessionKey)){
            return redirect('/');
        }
        $response = $next($request);
        return $response;
    }
}

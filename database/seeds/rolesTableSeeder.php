<?php

use Illuminate\Database\Seeder;

class rolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            [
                'role_name' => 'Super Roles',
                'role_type' => 'super_admin',
            ],
            [
                'role_name' => 'Roles',
                'role_type' => 'admin',
            ],
            [
                'role_name' => 'User',
                'role_type' => 'user',
            ]
        ]);
    }
}

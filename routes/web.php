<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'LoginController@index');
Route::post('/login', 'LoginController@getLogin');
Route::get('/register', 'LoginController@addUserForm');
Route::post('/register', 'LoginController@addUser');
Route::get('/logout', "LoginController@logout");
Route::get('/active-user/{id}', 'LoginController@getActiveUser');

Route::group(['middleware' => ['authentication']], function () {
    Route::get('/dashboard', 'DashboardController@index');
    Route::get('/users', 'UsersController@index');
    Route::match(['post'], 'ajaxRequestUsers', 'UsersController@getAjaxUsersList')->name('ajaxRequestUsers');
    Route::get('/edit-user/{id}', 'UsersController@edit');
    Route::post('/edit-user', 'UsersController@update');
    Route::get('/delete-user/{id}', 'UsersController@destroy');

    Route::get('/company', 'CompanyController@index');
    Route::match(['post'], 'ajaxRequestCompany', 'CompanyController@getAjaxCompanyList')->name('ajaxRequestCompany');
    Route::get('/add-company', 'CompanyController@create');
    Route::post('/add-company', 'CompanyController@store');
    Route::get('/edit-company/{id}', 'CompanyController@edit');
    Route::post('/edit-company', 'CompanyController@update');
    Route::get('/delete-company/{id}', 'CompanyController@destroy');
});
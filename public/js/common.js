var Common = function () {

    var dataTable = function (tableId, options) {
        var tableObject = {};
        var filterColumn = [];
        try {
            var _options = {
                // 'dom': "<'row'<'col-sm-2'l><'col-sm-4'B><'col-sm-6'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
                'buttons': [],
                'pageLength': 10,
                'responsive': true,
                "processing": true,
                "serverSide": true,
                "deferRender": true,
                "pagingType": "full_numbers",
                "ajax": {
                    "type": 'POST',
                },
                "oLanguage": {
                    "sSearch": "",
                    "sLengthMenu": "Show _MENU_",
                    /*'oPaginate': {
                     'sFirst': '<a href="#" title="First"><i class="fa fa-angle-double-left"></i></a>',
                     'sLast': '<a href="#" title="Last"><i class="fa fa-angle-double-right"></i></a>',
                     'sNext': '<a href="#" title="Next"><i class="fa fa-angle-right"></i></a>',
                     'sPrevious': '<a href="#" title="Previous"><i class="fa fa-angle-left"></i></a>',
                     }*/
                },
                "aLengthMenu": [
                    [5, 10, 15, 50, 100, -1],
                    [5, 10, 15, 50, 100, "All"]
                ],
                'order': [[0, "desc"]],
                'searchDelay': 350,
            };

            var _options = $.extend(true, _options, options);
            if (typeof _options.ajax.url === 'undefined' || $.trim(_options.ajax.url) === '') {
                _options.ajax = undefined;
            }

            tableObject = tableId.dataTable(_options);
            var searchHeader = tableObject.find('thead tr.table-search');
            tableObject.DataTable().on('responsive-resize', function (e, datatable, columns) {
                /*
                 var count = columns.reduce(function (a, b) {
                 return b === false ? a + 1 : a;
                 }, 0);
                 */
                searchHeader.children().each(function (index, obj) {
                    if (!columns[index]) {
                        $(this).hide();
                    } else {
                        $(this).show();
                    }
                });
            });
            // tableObject.DataTable().on('responsive-display', function (e, datatable, row, showHide, update) {
            //     console.log('Details for row ' + row.index() + ' ' + (showHide ? 'shown' : 'hidden'));
            // });
            tableObject.DataTable().on('column-sizing.dt', function (e, settings) {
                searchHeader.children().each(function (index, obj) {
                    $(obj).find('input:visible').css('width', '100%');
                });
            });
            tableObject.on('keyup change', '.table-column-search', function (e) {

                var that = $(this);
                var searchText = this.value.trim().replace(/(;|,)\s?/g, "");
                tableObject.find('thead tr.table-search').children().each(function (index, obj) {

                    if ($(obj).children().is(that)) {
                        filterColumn[index] = searchText;
                        return false;
                    }
                });
                var dataTableObj = tableObject.DataTable();

                for (var index in filterColumn) {
                    dataTableObj.columns(index).search(filterColumn[index]);
                }

                if($(this).is("input")){
                    if(e.keyCode == 13){
                        dataTableObj.draw();
                    }
                }else{
                    dataTableObj.draw();
                }

            });
            // $('body').on('click', '.filterColumnApply', function (e) {
            //     // tableObject.DataTable().columns(1).search('v').columns(2).search('v').draw();
            //     var dataTableObj = tableObject.DataTable();
            //     for (var index in filterColumn) {
            //         dataTableObj.columns(index).search(filterColumn[index]);
            //     }
            //     dataTableObj.draw();
            //     // tableObject.fnFilter(searchText, index);
            // });
        } catch (e) {
            //console.log(e);
            return;
        } finally {
            if (tableObject) {
                return tableObject;
            }
        }
    };

    var ajxsetup = function (config) {
        try {
            $.ajaxSetup({
                headers: {'X-CSRF-Token': $('input[name="_token"]').val()}
            });
        } catch (e) {
            // console.log(e);
        }
    };

    return {
        dataTable: function (TableID, options) {
            return dataTable(TableID, options);
        },
        ajxsetup: function (options) {
            ajxsetup(options);
        },
    };
}();
Common.ajxsetup();

setTimeout(function(){
    $('[id$=success]').fadeOut();
},4000);
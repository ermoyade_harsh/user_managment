$(document).ready(function(){
    $('body').on('change', '#statistics', function () {
        var statistics = $('#statistics').val();
        var _token = $('meta[name="csrf-token"]').attr('content')
        $('#filterData').html("");
        $.ajax({
            method: 'POST',
            url: base_path + '/get-department-employee-summary',
            data: {statistics:statistics, _token:_token},
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(result){
                $('#filterData').html(result);
            },
            error: function(jqXHR, textStatus, errorThrown) {
            }
        });
    });
});





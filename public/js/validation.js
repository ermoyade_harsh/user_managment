$(document).ready(function(){
    //validation
    $(function () {
        $('#validation-form-demo').parsley().on('field:validated', function () {
            var ok = $('.parsley-error').length === 0;
            $('.callout-info').toggleClass('hidden', !ok);
            $('.callout-danger').toggleClass('hidden', ok);
        })
            .on('form:submit', function () {
                return true; // Don't submit form for this demo
            });
    });

    $('#dob').datetimepicker({yearOffset: 0,lang: 'ch',timepicker: false,format: 'Y-m-d',formatDate: 'Y-m-d',});

    $("#photo").change(function(){
        $('.previewImageWrapper').html('');
        $('.previewImageWrapper').append("<img src='"+URL.createObjectURL(event.target.files[0])+"' class='img-thumbnail rounded-0' style='height: 150px;width: 197px;margin-right: 5px;'>");
    });
});

window.Parsley.addValidator('maxFileSize', {
    validateString: function(_value, maxSize, parsleyInstance) {
        var files = parsleyInstance.$element[0].files;
        return files.length != 1  || files[0].size <= maxSize * 2048;
    },
    requirementType: 'integer',
    messages: {
        en: 'Profile Image should not be larger than 2 MB.',
    }
});
window.Parsley.addValidator('maxFileSize1', {
    validateString: function(_value, maxSize, parsleyInstance) {
        var Extension = _value.split('.').pop().toLowerCase();
        if (Extension == "png" || Extension == "jpeg" || Extension == "jpg") {
            return true;
        }else {
            return false;
        }
    },
    messages: {
        en: 'Only allows file types of PNG, JPG, JPEG.'
    }
});
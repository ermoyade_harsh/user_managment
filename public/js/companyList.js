$(document).ready(function(){
    $('body').on('click', '.deleteCompany', function () {
        const companyId = $(this).attr('data-id');
        swal({
                title: "Are you sure?",
                text: "you want to delete this Company ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        method: 'GET',
                        url: base_path + '/delete-company/' + companyId ,
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function (response) {

                            if (response === "1") {
                                toastr.success("Company delete Successfull");
                                toastr.options.progressBar = true;
                            } else {
                                toastr.error("Something went wrong. please try again");
                                toastr.options.progressBar = true;
                            }
                            $("#company-table").DataTable().draw();
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                        }
                    });
                }
            });
    });

    var tableObj = {};

    var TableDatatablesAjaxUsers = function () {
        var init = function () {
            var t = $("#company-table");
            var config = {
                ajax: {
                    "url": ajaxRequestCompany,
                    method: 'POST'
                },
                aoColumns: [
                    {data: 'c.id', name: 'c.id'},
                    {data: 'c.logo', name: 'c.logo'},
                    {data: 'u.name', name: 'u.name'},
                    {data: 'c.name', name: 'c.name'},
                    {data: 'c.email', name: 'c.email'},
                    {data: 'c.contact_number', name: 'c.contact_number'},
                ],
                'order': [[0, "DESC"]],
                destroy: true,
                "bAutoWidth": false,

                "aoColumnDefs": [
                    {
                        "aTargets": [0],
                        "mRender": function (data, type, full) {
                            return full[0];
                        }
                    },
                    {
                        "aTargets": [1],
                        "mRender": function (data, type, full) {
                            var logo = full[1];
                            var string = '<img src="'+base_path+'/upload/'+logo +'" style="height: 151px;"/>';

                            return string;
                        }
                    },
                    {
                        "aTargets": [2],
                        "mRender": function (data, type, full) {
                            return full[2];
                        }
                    },
                    {
                        "aTargets": [3],
                        "mRender": function (data, type, full) {
                            return full[3];
                        }
                    },
                    {
                        "aTargets": [4],
                        "mRender": function (data, type, full) {
                            return full[4];
                        }
                    },
                    {
                        "aTargets": [5],
                        "mRender": function (data, type, full) {
                            return full[5];
                        }
                    },
                    {
                        "aTargets": [6],
                        "sortable":false,
                        "sWidth": "100px",
                        "mRender": function (data, type, full) {
                            var id = full[0];
                            var string = '<a href="'+ base_path+'/edit-company/'+ id+' " class="btn-sm btn btn-primary">Edit</a>  <a href="javascript:;" data-id="'+ id +'" class="btn-sm btn btn-danger deleteCompany">Delete</a>';
                            return string;
                        }
                    }
                ]
            };

            tableObj = Common.dataTable(t, config);
            columnSearch(t);
        };

        var columnSearch = function (tableId) {
            var searchHeader = tableId.find('thead tr.table-search');
            searchHeader.children().each(function (index, element) {
                var title = $(this).text()
                if ((index !== 1)  && (index !== 6)) {
                    $(this).html($('<input/>', {
                        'type': 'text',
                        'id': 'col_' +index,
                        'placeholder': 'Search ' + title
                    }).addClass('form-control table-column-search'));

                    $(this).find('input').css('width', '100%');
                }else {
                    $(this).html('');
                }
            });
        };
        return {
            init: function () {
                jQuery().dataTable && (!tableObj.hasOwnProperty('api')) && init();
            },
        }
    }();
    jQuery(document).ready(function () {
        TableDatatablesAjaxUsers.init()
    });
});
